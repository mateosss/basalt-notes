# Basalt Optimization Pseudocode

```cpp
class LinearizationBase {
 public:
  struct Options {
    LandmarkBlock::Options lb_options;
    LinearizationType linearization_type;
  };

  float linearizeProblem(bool* out_numerically_valid);
  void performQR();
  float backSubstitute(const VecX& pose_inc);
  void get_dense_Q2Jp_Q2r(MatX& Q2Jp, VecX& Q2r) const;
  void get_dense_H_b(MatX& H, VecX& b) const;

  static LinearizationBase* create(BundleAdjustmentBase* estimator, const AbsOrderMap& aom,
                                   const Options& options,
                                   const MargLinData<Scalar>* marg_lin_data = nullptr,
                                   const ImuLinData<Scalar>* imu_lin_data = nullptr,
                                   const std::set<FrameId>* used_frames = nullptr,
                                   const std::unordered_set<KeypointId>* lost_landmarks = nullptr,
                                   int64_t last_state_to_marg = std::numeric_limits<int64_t>::max());
};

class LinearizationAbsQR : LinearizationBase{
  umap<TimeCamId, size_t> host_to_idx_; // Arbitrary order of host kfs (in where?)
  using HostLandmarkMap = umap<TimeCamId, [LandmarkBlock*]>; //
  HostLandmarkMap host_to_landmark_block; //
  struct RelPoseLin {
    Mat4 T_t_h;
    Mat6 d_rel_d_h; // Deriv of T_ct_ch w.r.t. T_w_ih, see issue basalt#37
    Mat6 d_rel_d_t; // Deriv of T_ct_ch w.r.t. T_w_it, see issue basalt#37
  };
  using PoseLinMap = eumap<pair<TimeCamId, TimeCamId>, RelPoseLin>;
  PoseLinMap relative_pose_lin;
  vector<vector<PoseLinMap*>> relative_pose_per_host;
  vector<KeypointId> landmark_ids;
  vector<size_t> landmark_block_idx;
  size_t num_rows_Q2r;

  vector<ImuBlock*> imu_blocks;

  Scalar pose_damping_diagonal;
  Scalar pose_damping_diagonal_sqrt;
  VecX marg_scaling;

};

class LandmarkBlockAbsDynamic {
  // Dense storage for pose Jacobians, padding, landmark Jacobians and
  // residuals [J_p | pad | J_l | res]
  RowMatXf storage;

  Vec3 Jl_col_scale = Vec3::Ones();
  vector<JacobiRotation> damping_rotations; // TODO@mateosss: Can't I just store a single composed JacobiRotation instead of a vector with each?

  vector<RelPoseLin*> pose_lin_vec;
  vector<pair<TimeCamId, TimeCamId>*> pose_tcid_vec;
  size_t padding_idx = 0;
  size_t padding_size = 0;
  size_t lm_idx = 0;
  size_t res_idx = 0;

  size_t num_cols = 0;
  size_t num_rows = 0;

  Options* options_ = nullptr;

  State state = State::Uninitialized;

  Landmark* lm_ptr = nullptr;
  Calibration* calib_ = nullptr;
  AbsOrderMap* aom_ = nullptr;

  map<int64_t, set<int>> res_idx_by_abs_pose_; // frame -> {res_idx}
};

class ImuBlock {
  array<FrameId, 2> frame_ids;
  Mat15x30 Jp;
  Vec15 r;
  IntegratedImuMeasurement* imu_meas;
}

struct MargData {
  AbsOrderMap aom;
  MatrixXd abs_H;
  VectorXd abs_b;
  map<int64_t, PoseVelBiasStateWithLin<double>> frame_states;
  map<int64_t, PoseStateWithLin<double>> frame_poses;
  set<int64_t> kfs_all;
  set<int64_t> kfs_to_marg;
  bool use_imu;

  vector<OpticalFlowResult*> opt_flow_res;
};

void optimize() {
  if (!opt_started || frame_states.size() <= 4) return;
  opt_started = true;

  AbsOrderMap aom; // Order of states in linear system -> sort by timestamp
  for ({t, pose} : frame_poses) { // Keyframe poses (s_k), empty before marginalize.
    aom.abs_ordder_map[t] = {aom.total_size, POSE_SIZE};
    aom.total_size += POSE_SIZE; // 6
    aom.items++;
  }
  for ({t, state} in frame_states) { // Frame poses (s_f)
    aom.abs_order_map[t] = {aom.total_size, POSE_VEL_BIAS_SIZE};
    aom.total_size += POSE_VEL_BIAS_SIZE; // 15
    aom.items++;
  }

  // setup landmark blocks
  LinnearizationBase::Options lqr_options;
  lqr_options.lb_options.huber_parameter = config.huber_thresh; // 1.0
  lqr_options.lb_options.obs_std_dev = config.obs_std_dev; // 0.5
  lqr_options.linearization_type = config.vio_linearization_type; // ABS_QR
  ImuLinData ild = {g, gyro_bias_sqrt_weight, accel_bias_sqrt_weight, {}};
  for ({t, m} : imu_meas) ild.imu_meas[t] = &imu_meas[t]; // t -> IntegratedImuMeasurement
  LinearizationBase* lqr = LinearizationBase::create(this, aom, lqr_options, &marg_data, &ild) { // MARK
    new LinearizationAbsQR(this, aom, lqr_options, marg_lin_data=marg_data, ild) {
      // Copies and references the arguments to the object

      // Allocate memory for relative pose linearization
      // TODO@mateosss: warning getOBservations is an unordered map but is iterated a couple of time, is iteration order guaranteed to remain constant?
      host_to_idx_ = {tcid_h: i for i, {tcid_h, _} in enumerate(lmdb.getObservations())};
      host_to_landmark_block = {tcid_h: [] for {tcid_h, _} in lmdb.getObservations()};
      relative_pose_lin = [
          {{tcid_h, tcid_t}: RelPoseLin() for tcid_t, obs in target_map}
          for {tcid_h, target_map} in lmdb.getObservations()
      ]
      relative_pose_per_host = [
        [&relative_pose_lin[tcid_h, tcid_t] for tcid_t, _ in target_map]
        for tcid_h, target_map in lmdb_.getObservations()
      ];

      num_cameras = frame_poses.size();

      if (used_frames || lost_landmarks) landmark_ids = [lmid for lmid, lm in lmdb.getLandmarks() if lm.host_kf.ts in used_frames or lmid in lost_landmarks];
      else landmark_ids = [k for k, v in lmdb.getLandmarks()];
      size_t num_landmarks = landmark_ids.size();
      landmark_blocks.resize(num_landmarks);

      for@parallel(int r = 0; r < num_landmarks; r++) {
        lmid, lb = landmark_ids[r], &landmark_blocks[r];
        lm = lmdb.getLandmark(lmid);
        lb = LandmarkBlock::createLandmarkBlock();
        lb->allocateLandmark(lm, relative_pose_lin, calib, aom, options.lb_options) { // MARK
          pose_lin_vec = [0] * lm.obs.size();
          pose_tcid_vec = [0] * lm.obs.size();

          for ([tcid_t, pos] : lm.obs) {
            size_t i = pose_lin_vec.size();
            [pair<TimeCamId, TimeCamId> ht, RelPoseLin& rpl] = relative_pose_lin[lm.host_kf_id, tcid_t];
            pose_lin_vec.push_back(&rpl if tcid_t.frame_id in aom.abs_order_map else NULL);
            pose_tcid_vec.push_back(&ht);
            res_idx_by_abs_pose_[ht.host.frame_id].insert(i);    // host
            res_idx_by_abs_pose_[ht.target.frame_id].insert(i);  // target
          }

          num_rows = pose_lin_vec.size() * 2 + 3;  // residuals and lm damping

          padding_idx = aom->total_size; // number of pose-jacobian columns is determined by aom
          padding_size = padding_idx % 4 != 0 ? (4 - padding_idx % 4) : 0;
          lm_idx = padding_idx + padding_size; // multiple of 4 after padding
          res_idx = lm_idx + 3;
          num_cols = res_idx + 1;

          storage.resize(num_rows, num_cols);

          damping_rotations.clear();
          damping_rotations.reserve(6);

          state = State::Allocated;
        };
      }

      num_rows_Q2r = 0;
      for (size_t i = 0; i < num_landmarks; i++) {
        landmark_block_idx.push_back(num_rows_Q2r);
        const auto& lb = landmark_blocks[i];
        num_rows_Q2r += lb->numQ2rows();
        host_to_landmark_block[lb->getHostKf()].push_back(lb);
      }

      if (imu_lin_data) // false for VO
        for (auto [ts, pim] : imu_lin_data->imu_meas)
          imu_blocks.emplace_back(new ImuBlock(pim, imu_lin_data, aom));
    }
  }

  lambda = config.vio_lm_lambda_initial; // 1e-4
  bool terminated = false;
  bool converged = false;
  std::string message;

  int it = 0;
  int it_rejected = 0;
  for (; it <= config.vio_max_iterations(7) && !terminated;) {
    float error_total, bool numerically_valid = lqr->linearizeProblem() { // linearize residuals // MARK
      // reset damping and scaling (might be set from previous iteration)
      pose_damping_diagonal = 0;
      pose_damping_diagonal_sqrt = 0;
      marg_scaling = VecX();

      // Linearize relative poses
      for (auto [tcid_h, target_map] : lmdb_.getObservations()) {
        for ([tcid_t, _] : target_map) {
          RelPoseLin<Scalar>& rpl = relative_pose_lin.at({tcid_h, tcid_t});

          if (tcid_h != tcid_t) {
            PoseStateWithLin state_h = estimator->getPoseStateWithLin(tcid_h.frame_id);
            PoseStateWithLin state_t = estimator->getPoseStateWithLin(tcid_t.frame_id);

            // compute relative pose & Jacobians at linearization point
            SE3 T_t_h = computeRelPose(h = state_h.getPoseLin(), t = state_t.getPoseLin(), calib, &rpl) {
              SE3 T_w_ih = state_h.getPoseWithLin();
              SE3 T_w_it = state_t.getPoseWithLin();
              SE3 T_i_ch = calib.T_i_c[state_h.cam_id];
              SE3 T_i_ct = calib.T_i_c[state_t.cam_id];

              SE3 T_ct_ch = T_i_ct.inverse() * T_w_it.inverse() * T_w_ih * T_i_ch;
              SE3 T_ct_ih = T_i_ct.inverse() * T_w_it.inverse() * T_w_ih;
              SE3 T_ct_it = T_i_ct.inverse();

              // TODO@mateosss: There is likely to be many observations that share the samepair of
              // host-target keyframes. In those cases we are recompunting the same rpl multiple times

              // Discussion: https://gitlab.com/VladyslavUsenko/basalt/-/issues/37
              Matrix3 Rh = T_w_ih.so3().inverse().matrix();
              Matrix6 RRh = [Rh 0 | 0 Rh]; // notation: pipe is newline
              if (rpl) rpl.d_rel_d_h = T_ct_ih.Adj() * RRh; // Deriv of T_ct_ch w.r.t. T_w_ih

              Matrix3 Rt = T_w_it.so3().inverse().matrix();
              Matrix6 RRt = [Rt 0 | 0 Rt];
              if (rpl) rpl.d_rel_d_t = -T_ct_it.Adj() * RRt; // Deriv of T_ct_ch w.r.t. T_w_it

              return T_ct_ch;
            };

            // if either already linearized, then current state guess is different from the state
            // at which linearization happened. We need to recompute T_t_h from the current guess.
            if (state_h.isLinearized() || state_t.isLinearized()) // if either is linearized then current state estimate
              T_t_h = computeRelPose(h = state_h.getPose(), t = state_t.getPose(), calib);

            rpl.T_t_h = T_t_h.matrix();
          } else {
            rpl.T_t_h.setIdentity();
            rpl.d_rel_d_h.setZero();
            rpl.d_rel_d_t.setZero();
          }
        }
      }

      float error = 0;
      bool valid = true;

      // Linearize landmarks
      for@parallel(lb : landmark_blocks) {
          error += lb->linearizeLandmark() { // may set NumericalFailure
            clear(storage, damping_rotations);

            bool numerically_valid = true
            float error_sum = 0;

            for (auto i, [tcid_t, obs] in enumerate(lm_ptr->obs)) {
                if (pose_lin_vec[i] == nullptr) continue;
                GenericCamera cam_t = calib.intrinsic[tcid_t.cam_id];
                int64_t h, t = pose_tcid_vec[i][0, 1].t_ns;
                size_t h_idx = aom[h].idx;
                size_t t_idx = aom[t].idx;
                SE3 T_t_h = pose_lin_vec[i].T_t_h;
                Mat6 d_rel_d_t = pose_lin_vec[i].d_rel_d_t;

                Vec2 res; // Reprojection error in cam_t of the landmark hosted in cam_h
                Matrix2x6 d_res_d_xi; // xi is 6dof pose, maybe it is the pose difference betwee cam t and h
                Matrix2x3 d_res_d_p; // p is 3d point
                bool valid = linearizePoint(obs, *lm_ptr, T_t_h, cam_t, &res, &d_res_d_xi, &d_res_d_p); // See prev 3 comments

                if (options.use_valid_projections_only && !valid) continue;

                numerically_valid &= d_res_d_xi.allFinite() && d_res_d_p.alllFinite();

                float [weighted_error, weight] = compute_error_weight(res**2) {
                  float p = options.huber_parameter;
                  if (p > 0) {
                    float w = res**2 <= p**2 ? 1 : p / res; // Huber weight
                    float e = 0.5 * (2 - w) * w * res**2;
                    return {e, w};
                  } else { // Use squared norm
                    return {0.5 * res**2, 1};
                  }
                }
                float stdev = options.obs_std_dev; // 0.5
                float sqrt_weight = sqrt(weight) / stdev

                error_sum += weighted_error / (stdev**2);

                size_t obs_idx = i * 2;
                storage.block<2, 3>(obs_idx, lm_idx) = sqrt_weight * d_res_d_p;
                storage.block<2, 1>(obs_idx, res_idx) = sqrt_weight * res;
                storage.block<2, 6>(obs_idx, h_idx) += sqrt_weight * d_res_d_xi * d_rel_d_h;
                storage.block<2, 6>(obs_idx, t_idx) += sqrt_weight * d_res_d_xi * d_rel_d_t;

            }
            state = numerically_valid ? State::Linearized : State::NumericalFailure;
            return error_sum;
          }
          valid &= !lb->isNumericalFailure();
      }

      if (numerically_valid) *numerically_valid = valid;

      // Linearize IMU data
      if (imu_lin_data) // false for VO
        for (auto& ib : imu_blocks) error += ib->linearizeImu(estimator->frame_states) {
          Jp.setZero();
          r.setZero();

          int64_t start_t = imu_meas->get_start_t_ns();
          int64_t end_t = imu_meas->get_start_t_ns() + imu_meas->get_dt_ns();

          PoseVelBiasStateWithLin start_state = frame_states.at(start_t);
          PoseVelBiasStateWithLin end_state = frame_states.at(end_t);

          Mat9x9 d_res_d_start, d_res_d_end; // 9 for POSE_VEL_SIZE
          Mat9x3 d_res_d_bg, d_res_d_ba;

          Vec9 res = imu_meas->residual(
              start_state.getStateLin(), imu_lin_data->g, end_state.getStateLin(), start_state.getStateLin().bias_gyro,
              start_state.getStateLin().bias_accel, &d_res_d_start, &d_res_d_end, &d_res_d_bg, &d_res_d_ba) {
                Vec9 res; // see section III.b.3 of main paper
                res.segment<3>(0) = ...; // translation residual
                res.segment<3>(3) = ...; // rotation residual
                res.segment<3>(6) = ...; // velocity residual
                ...; // compute derivatives d_res_d_{start, end, bg, ba}
                return res;
            }

          if (start_state.isLinearized() || end_state.isLinearized()) {
            res = imu_meas->residual(start_state.getState(), imu_lin_data->g, end_state.getState(),
                                    start_state.getState().bias_gyro, start_state.getState().bias_accel);
          }

          // error
          float dt = imu_meas->get_dt_ns() * 1e-9;
          Vec3 gyro_bias_weight_dt = imu_lin_data->gyro_bias_weight_sqrt / sqrt(dt);
          Vec3 accel_bias_weight_dt = imu_lin_data->accel_bias_weight_sqrt / sqrt(dt);
          Vec3 res_bg = start_state.getState().bias_gyro - end_state.getState().bias_gyro;
          Vec3 res_ba = start_state.getState().bias_accel - end_state.getState().bias_accel;
          float bg_error = 0.5 * (gyro_bias_weight_dt.asDiagonal() * res_bg).squaredNorm();
          float ba_error = 0.5 * (accel_bias_weight_dt.asDiagonal() * res_ba).squaredNorm();
          float imu_error = 0.5 * (imu_meas->get_sqrt_cov_inv() * res).squaredNorm();

          // imu residual linearization
          Jp.block<9, 9>(0, 0) = imu_meas->get_sqrt_cov_inv() * d_res_d_start;
          Jp.block<9, 3>(0, 9) = imu_meas->get_sqrt_cov_inv() * d_res_d_bg;
          Jp.block<9, 3>(0, 12) = imu_meas->get_sqrt_cov_inv() * d_res_d_ba;
          Jp.block<9, 9>(0, 15) = imu_meas->get_sqrt_cov_inv() * d_res_d_end;
          Jp.block<3, 3>(9, 9) = gyro_bias_weight_dt.asDiagonal();
          Jp.block<3, 3>(9, 24) = (-gyro_bias_weight_dt).asDiagonal();
          Jp.block<3, 3>(12, 12) = accel_bias_weight_dt.asDiagonal();
          Jp.block<3, 3>(12, 27) = (-accel_bias_weight_dt).asDiagonal();

          r.segment<9>(0) = imu_meas->get_sqrt_cov_inv() * res;
          r.segment<3>(9) += gyro_bias_weight_dt.asDiagonal() * res_bg;
          r.segment<3>(12) += accel_bias_weight_dt.asDiagonal() * res_ba;

          return imu_error + bg_error + ba_error;
        };

      if (marg_lin_data)
        error += estimator->computeMargPriorError(*marg_lin_data) { // MARK
          maom, H, b = marg_lin_data.{order, H, b};
          VecX delta = computeDelta(maom) { // MARK
            VecX d = Zeros(maom.total_size);
            for ({t, {idx, size}} : maom) {
              auto s = frame_poses if size == POSE_SIZE else frame_states;
              assert(s[t].isLinearized());
              d.segment<size>(idx) = s[t].getDelta(); // delta of current solver iteration estimate to initial estimate
            }
            return d;
          };
          if (marg_lin_data.is_sqrt) return delta.T * H.T * (0.5 * H * delta + b)
          else return delta.T * (0.5 * H * delta + b)
        };

      return error;
    }

    lqr->performQR() { // marginalize points in place // MARK
      for@parallel(lb : landmark_blocks) {
        lb->performQR() {
          assert(state == State::Linearized);
          performQRHouseholder() { // By default it is householder but can use givens rotation too
            // NOTE: I think this turns the 3 landmark columns into upper triangular while modifying things.
            // But that doesnt really makes sense, does it?
            VecX v1(num_cols);
            VecX v2(num_rows - 3);
            for (size_t k = 0; k < 3; ++k) {
              size_t remainingRows = (num_rows - 3) - k;
              Scalar tau, beta;
              storage.col(lm_idx + k).segment(k, remainingRows).makeHouseholder(v2, tau, beta);
              storage.block(k, 0, remainingRows, num_cols).applyHouseholderOnTheLeft(v2, tau, v1.data());
            }
          };
          state = State::Marginalized;
        };
      }
    };

    for (int j = 0; it <= config.vio_max_iterations(7) && !terminated; j++) { // inner loop for backtracking in LM

      VecX inc; // Compute increment
      MatX H, VecX b = lqr->get_dense_H_b(H, b) { // get dense reduced camera system // MARK
        H = Zeros(aom.total_size, aom.total_size);
        b = Zeros(aom.total_size);
        for@parallel(int r = 0; r < num_landmarks; r++) {
          landmark_blocks[r]->add_dense_H_b(H, b) {
            VecX r = storage.col(res_idx).tail(num_rows - 3);
            MatX J = storage.block(3, 0, num_rows - 3, aom.total_size);
            H += J.transpose() * J;
            b += J.transpose() * r;
          };
        };
        if (imu_lin_data) add_dense_H_b_imu(H, b) {
          for (ib : imu_blocks) {
            ib->add_dense_H_b(H, b) {
              int64_t start_t = imu_meas->get_start_t_ns();
              int64_t end_t = imu_meas->get_start_t_ns() + imu_meas->get_dt_ns();

              size_t start_idx = aom.abs_order_map.at(start_t).idx;
              size_t end_idx = aom.abs_order_map.at(end_t).idx;

              MatX HH = Jp.transpose() * Jp;
              VecX bb = Jp.transpose() * r;

              int P = POSE_VEL_BIAS_SIZE;
              H.block<P, P>(start_idx, start_idx) += HH.block<P, P>(0, 0);
              H.block<P, P>(end_idx, start_idx) += HH.block<P, P>(P, 0));
              H.block<P, P>(start_idx, end_idx) += HH.block<P, P>(0, P));
              H.block<P, P>(end_idx, end_idx) += HH.block<P, P>(P, P));
              b.segment<P>(start_idx) += bb.segment<P>(0);
              b.segment<P>(end_idx) += bb.segment<P>(P);
            }
          }
        };
        // add_dense_H_b_pose_damping(H) { H.diagonal() += pose_damping_diagonal; } // pose damping disabled
        add_dense_H_b_marg_prior(H, b) { estimator->linearizeMargPrior(H, b, mld = marg_lin_data) {
          // NOTE: See useful explanatory comment in codebase
          VecX delta = computeDelta(mld.order, delta); // See above usages for def
          int M = mld.order.total_size;
          if (mld.is_sqrt) {
            H.topLeftCorner(M, M) += mld.H.T * mld.H;
            b.head(M) += mld.H.T * (mld.b + mld.H * delta);
            return delta.T * H.T * (0.5 * H * delta + b)
          } else {
            H.topLeftCorner(M, M) += mld.H;
            b.head(M) += mld.H * delta + mld.b;
            return delta.T * (0.5 * H * delta + b)
          }
        }}
      }
      bool inc_valid = false;
      for (int iter = 0; iter < 3 && !inc_valid; iter++) {
        VecX Hdiag_lambda = (H.diagonal() * lambda).cwiseMax(min_lambda);
        MatX H_copy = H;
        H_copy.diagonal() += Hdiag_lambda;

        Eigen::LDLT<Eigen::Ref<MatX>> ldlt(H_copy); // MARK
        inc = ldlt.solve(b);
        inc_valid = inc.array().isFinite().all();
        if (!inc_valid) {
          lambda *= lambda_vee;
          lambda_vee *= vee_factor;
        }
      }

      backup() { // backup state (then apply increment and check cost decrease)
        for ([t, state] : frame_states) state.backup();
        for ([t, pose] : frame_poses) state.backup();
        lmdb.backup();
      };

      // backsubstitute (with scaled pose increment)
      inc = -inc; // negate pose increment before point update
      float l_diff = lqr->backSubstitute(inc) { // MARK
        // See issue https://gitlab.com/VladyslavUsenko/basalt/-/issues/61
        Scalar l_diff = 0;
        for@parallel(lb : landmark_blocks) l_diff -= lb->backSubstitute(incp = inc) { // MARK
          assert(state == State::Marginalized);

          // Compute landmark delta
          MatXx3 R = storage.block<X, 3>(0, lm_idx); // [R1 0]
          Mat3x3 R1 = R.triangle<3, 3>(0, 0); // "Q1Jl" in code
          Vec3 Q1Tr = storage.col(res_idx).head<3>(); // "Q1Jr" in code
          Mat3xX Q1TJp = storage.topLeftCorner(3, padding_idx); // "Q1Jp" in code
          Vec3 incl = -R1.solve(Q1Tr + Q1TJp * incp); // landmark increment // Eq. 16 in rootba paper

          // Compute linearized model cost change (i.e., expected cost change): l_diff = E(x₀) - E'(x₀ + Δx)
          // setLandmarkDamping(0) { // Damping disabled
          MatX QTJp = storage.topLeftCorner(num_rows - 3, padding_idx); // Q^T Jp
          VecX QTr = storage.col(res_idx).head(num_rows - 3); // "Qr" in code
          VecX QT_J_inc = QTJp * incp + R * incl; // "QJinc" in code
          return QT_J_inc.T * (0.5 * QT_J_inc + QTr);

          // Apply landmark delta
          if (!finite(incl) || !finite(lm[u, v, invdepth])) print("Numerical failure in backsubstitution");
          // incl *= Jl_col_scale; // Damping disabled
          lm_ptr->direction += incl[0, 1]
          lm_ptr->inv_dist = max(lm_ptr->inv_dist + incl[2], 0);
        }

        if (imu_lin_data) for (ib : imu_blocks) l_diff -= ib->backSubstitute(incp = inc) { // MARK
          int64_t a_t = imu_meas->get_start_t_ns();
          int64_t b_t = imu_meas->get_start_t_ns() + imu_meas->get_dt_ns();
          size_t a_idx = aom[a_t].idx;
          size_t b_idx = aom[b_t].idx;

          // Compute linearized model cost change (i.e., expected cost change): l_diff = E(x₀) - E'(x₀ + Δx)
          int P = POSE_VEL_BIAS_SIZE; // 15
          Vec<2 * P> reduced_incp = incp.segment<P>(a_idx) ++ incp.segment<P>(b_idx);
          VecX Jpincp = Jp * reduced_incp; // "Jinc" in code
          return Jpincp.T * (0.5 * Jpincp + r);
        };

        if (marg_lin_data) {
          size_t marg_size = marg_lin_data->H.cols();
          VecX pose_inc_marg = pose_inc.head(marg_size);
          l_diff += estimator->computeMargPriorModelCostChange(*marg_lin_data, marg_scaling, pose_inc_marg); // MARK // TODO@mateosss
        }

        return l_diff;
      };

      // apply increment to poses
      for ({t, state} : frame_poses) { // Keyframe poses
        int idx = aom.abs_order_map.at(t).first;
        state.applyInc(inc.segment<POSE_SIZE>(idx)); // TODO@mateosss: 7 linearized vs !linearized
      }

      for ({t, state} : frame_states) { // Frame posevelbiases
        int idx = aom.abs_order_map.at(frame_id).first;
        state.applyInc(inc.segment<POSE_VEL_BIAS_SIZE>(idx));
      }

      float step_norminf = inc.array().abs().maxCoeff(); // compute stepsize

      // compute error update applying increment
      float after_update_vision_and_inertial_error = computeError(vector<pair<TimeCamId, float>> outliers = NULL) { // MARK
        if (outliers) outliers.clear();
        float error = 0;
        for@parallel({tcid_h, tcid_t, lmid} : lmdb.getObservations()) {
          Landmark lm = lmdb.getLandmark(lmid);
          Vec2 obs = lm.obs[tcid_t];
          Mat4 T_t_h = tcid_h != tcid_t ? computeRelPose(tcid_t, tcid_h, calib) : Mat4::Identity();
          [bool valid, Vec2 res] = linearizePoint(obs, lm, T_t_h, cam_for(tcid_t));
          if (valid) {
            float e = res.norm();
            float huber_weight = e < huber_thresh ? 1.0 : huber_thresh / e;
            float obs_weight = huber_weight / (obs_std_dev * obs_std_dev);
            error += 0.5 * (2 - huber_weight) * obs_weight * e**2;
            if (outliers && e > outlier_threshold) outliers.push({tcid_t, e if tcid_h != tcid_t else -2})
          } else {
            if (outliers) outliers_concurrent.push({tcid_t, -1 if tcid_h != tcid_t else -2});
          }
        }
      };
      float after_update_marg_prior_error = computeMargPriorError(marg_data); // TODO@mateosss: 9 // MARK
      float [after_update_imu_error, after_bg_error, after_ba_error] = ScBundleAdjustmentBase::computeImuError(
        aom, frame_states, imu_meas, bgw = gyro_bias_sqrt_weight.square(), baw = accel_bias_sqrt_weight.square(), g
      ) { // MARK
        float imu_error = 0;
        float bg_error = 0;
        float ba_error = 0;
        for ({ts, pim} : imu_meas) {
          if (pim.get_dt_ns() == 0) continue;

          int64_t a_t, b_t = pim.get_start_t_ns(), a_t + pim.get_dt_ns();
          if (a_t not in aom or b_t not in aom) continue;

          PoseVelBiasStateWithLin a, b = frame_states[a_t, b_t];
          float dt_s = pim.get_dt_ns() * 1e-9;

          Vec9 res = pim.residual(a, g, b, a.bias_gyro, a.bias_accel);
          Vec3 res_bg = a.bias_gyro - b.bias_gyro;
          Vec3 res_ba = start_state.getState().bias_accel - end_state.getState().bias_accel;
          imu_error += 0.5 * res.T * pim.get_cov_inv * res;
          bg_error += 0.5 * res_bg.T * (bgw / dt).asDiagonal() * res_bg;
          ba_error += 0.5 * res_ba.T * (baw / dt).asDiagonal() * res_ba;
        }
        return {imu_error, bg_error, ba_error};
      }

      float after_error_total = (after_update_vision_and_inertial_error +
        after_update_imu_error + after_bg_error + after_ba_error + after_update_marg_prior_error);

      // check cost decrease compared to quadratic model cost
      bool step_is_valid = false;
      bool step_is_successful = false;
      float f_diff = error_total - after_error_total; // compute actual cost decrease
      float relative_decrease = f_diff / l_diff; // TODO@mateosss: What is l_diff

      step_is_valid = l_diff > 0; // This can happen due to floating point errors (close to -0.0f)
      step_is_successful = step_is_valid && relative_decrease > 0;

      if (step_is_successful) { // TODO@mateosss: 11
        lambda *= max(1.0 / 3, 1 - (2 * relative_decrease - 1)**3);
        lambda = max(min_lambda, lambda);
        lambda_vee = initial_vee;
        it++;

        // check function and parameter tolerance
        if ((f_diff > 0 && f_diff < 1e-6) || step_norminf < 1e-4) {
          converged = true;
          terminated = true;
        }

        break; // stop inner lm loop
      } else {
        string reason = step_is_valid ? "REJECTED" : "INVALID";
        lambda *= lambda_vee;
        lambda_vee *= vee_factor;

        restore();
        it++;
        it_rejected++;

        if (lambda > max_lambda) {
          terminated = true;
          message = "Solver did not converge and reached maximum damping lambda";
        }
      }
    }
  }
}

void marginalize(map<Timestamp, int> num_points_connected, set<KeypointId> lost_landmarks) {
  if (!opt_started) return;

  bool marg_kf = frame_poses.size() > vioconfig.max_kfs;
  bool marg_frame = frame_states.size() >= vioconfig.max_states;
  if (!marg_kf && !marg_frame) return; // Do not marginalize

  AbsOrderMap aom;
  set<int64_t> poses_to_marg = {ts for ts, _ in frame_poses if ts not in kf_ids}; // remove all frame_poses that are not kfs
  for ({ts, pose} : frame_poses) {
    aom.abs_order_map[ts] = (aom.total_size, POSE_SIZE); // POSE_SIZE=6
    aom.total_size += POSE_SIZE;
    aom.items++;
  }

  int states_to_remove = frame_states.size() - max_states + 1; // always at least 1
  int64_t last_state_to_marg = frame_states.begin()[states_to_remove].key();
  set<int64_t> states_to_marg_vel_bias;
  set<int64_t> states_to_marg_all;
  for ({ts, state} : frame_states) {
    if (ts > last_state_to_marg) break;

    if (ts != last_state_to_marg) {
      if (ts not in kf_ids) {
        states_to_marg_vel_bias.emplace(ts);
      } else {
        states_to_marg_all.emplace(ts);
      }
    }

    aom.abs_order_map[ts] = (aom.total_size, POSE_VEL_BIAS_SIZE); // POSE_VEL_BIAS_SIZE=15
    aom.total_size += POSE_VEL_BIAS_SIZE;
    aom.items++;
  }

  set<int64_t> kf_ids_all = kf_ids;
  set<int64_t> kfs_to_marg;
  while (kf_ids.size() > max_kfs && !states_to_marg_vel_bias.empty()) {
    int64_t id_to_marg = -1;

    // starting from the oldest kf (and skipping the newest 2 kfs), try to
    // find a kf that has less than a small percentage of its landmarks
    // tracked by the current frame
    if (kf_ids.size() > 2) {
      for (ts : kf_ids[:-2]) {
        if (ts not in num_points_connected || num_points_connected[ts] / num_points_kf[ts] < config.vio_kf_marg_feature_ratio(0.1)) {
          id_to_marg = ts;
          break;
        }
      }
    }

    // Note: This score function is taken from DSO, but it seems to mostly
    // marginalize the oldest keyframe. This may be due to the fact that
    // we don't have as long-lived landmarks, which may change if we ever
    // implement "rediscovering" of lost feature tracks by projecting
    // untracked landmarks into the localized frame.
    if (kf_ids.size() > 2 && id_to_marg < 0) {
      int64_t last_kf = kf_ids[-1];
      float min_score = MAX_FLOAT;
      int64_t min_score_id = -1;

      for (ts1 : kf_ids[:-2]) {
        // Big distance to other keyframes --> lower score
        float denom = 0;
        for (ts2 : kf_ids[:-2]) {
          dist_to_kf2 = (frame_poses[ts1].translation() - frame_poses[ts2].translation()).norm();
          denom += 1 / (dist_to_kf2 + 1e-5);
          // TODO@mateosss: if 1e-5 is only there to avoid div by 0, then just sum all denoms and add 1e-5 at the final score
        }

        // Small distance to latest kf --> lower score
        float dist_to_last_kf = (frame_poses[ts1].translation() - frame_states[last_kf].T_w_i.translation()).norm();
        float score = sqrt(dist_to_last_kf) * denom;

        if (score < min_score) min_score, min_score_id = score, ts1;
      }

      id_to_marg = min_score_id;
    }

    kfs_to_marg.push(id_to_marg);
    poses_to_marg.push(id_to_marg);
    kf_ids.erase(id_to_marg);
  }

  bool is_sqrt = isLinearizationSqrt(config.vio_linearization_type) && marg_data.is_sqrt;

  MatX Q2Jp_or_H;
  VecX Q2r_or_b;

  // Linearize
  LinearizationBase::Options lqr_options;
  lqr_options.lb_options.huber_parameter = huber_thresh;
  lqr_options.lb_options.obs_std_dev = obs_std_dev;
  lqr_options.linearization_type = config.vio_linearization_type;
  ImuLinData ild = {g, gyro_bias_sqrt_weight, accel_bias_sqrt_weight, {}};
  ild.imu_meas = {t: &m for t, m in imu_meas if m.get_start_t_ns() in aom and m.get_end_t_ns() in aom};
  LinearizationBase* lqr = LinearizationBase::create(this, aom, lqr_options, &marg_data, &ild, used_frames = &kfs_to_marg, &lost_landmaks, last_state_to_marg);
  lqr->linearizeProblem();
  lqr->performQR();
  if (is_sqrt) lqr->get_dense_Q2Jp_Q2r(Q2Jp_or_H, Q2r_or_b); // MARK // TODO@mateosss
  else lqr->get_dense_H_b(Q2Jp_or_H, Q2r_or_b);

  // Save marginalization prior
  if (out_marg_queue && !kfs_to_marg.empty()) {
    MargData* m = new MargData;
    m->aom = aom;
    m->abs_H = is_sqrt ? Q2Jp_or_H.T * Q2Jp_or_H : Q2Jp_or_H;
    m->abs_b = is_sqrt ? Q2Jp_or_H.T * Q2r_or_b : Q2r_or_b;
    m->frame_poses = frame_poses;
    m->frame_states = frame_states;
    m->kfs_all = kf_ids_all;
    m->kfs_to_marg = kfs_to_marg;
    m->use_imu = true;
    m->opt_flow_res = [prev_opt_flow_res[t] for t in m->kfs_all];
    out_marg_queue->push(m);
  }

  set<int> idx_to_keep, idx_to_marg;
  for ((ts, (idx, size)) : aom.abs_order_map) {
    if (size == POSE_SIZE) {
      if (ts not in poses_to_marg) {
        idx_to_keep += range(idx, idx + POSE_SIZE);
      } else {
        idx_to_marg += range(idx, idx + POSE_SIZE);
      }
    } else { // assert(size == POSE_VEL_BIAS_SIZE);
      if (ts in states_to_marg_all) {
        idx_to_marg += range(idx, idx + POSE_VEL_BIAS_SIZE);
      } else if (ts in states_to_marg_vel_bias) {
        idx_to_keep += range(idx, idx + POSE_SIZE);
        idx_to_marg += range(idx + POSE_SIZE, idx + POSE_SIZE + POSE_VEL_BIAS_SIZE);
      } else { // assert(ts == last_state_to_marg);
        idx_to_keep += range(idx, idx + POSE_VEL_BIAS_SIZE)
      }
    }
  }

  if (config.vio_debug || config.vio_extended_logging) {} // MARK TODO@mateosss: nullspace_marg_data

  MatX marg_H_new;
  VecX marg_b_new;
  if (is_sqrt) marginalizeHelperSqrtToSqrt(Q2Jp_or_H, Q2r_or_b, idx_to_keep, idx_to_marg, marg_H_new, marg_b_new) // MARK // TODO@mateosss
  else if (marg_data.is_sqrt) marginalizeHelperSqToSqrt(Q2Jp_or_H, Q2r_or_b, idx_to_keep, idx_to_marg, marg_H_new, marg_b_new);
  else marginalizeHelperSqToSq(Q2Jp_or_H, Q2r_or_b, idx_to_keep, idx_to_marg, marg_H_new, marg_b_new);

  assert(!frame_states[last_state_to_marg].isLinearized());
  frame_states[last_state_to_marg].setLinTrue();

  for (int64_t ts : states_to_marg_all) {
    frame_states.erase(ts);
    imu_meas.erase(ts);
    prev_opt_flow_res.erase(ts);
  }

  for ( int64_t ts : states_to_marg_vel_bias) {
    frame_poses[ts] = frame_states[ts].asPose();
    frame_states.erase(ts);
    imu_meas.erase(ts);
  }

  for (int64_t ts : poses_to_marg) {
    frame_poses.erase(ts);
    prev_opt_flow_res.erase(ts);
  }

  lmdb.removeKeyframes(kfs_to_marg, poses_to_marg, states_to_marg_all);
  if (config.vio_marg_lost_landmarks) for (const auto& lm_id : lost_landmaks) lmdb.removeLandmark(lm_id);

  AbsOrderMap marg_order_new;
  for ({ts, pose} : frame_poses) {
    marg_order_new.abs_order_map[ts] = (marg_order_new.total_size, POSE_SIZE);
    marg_order_new.total_size += POSE_SIZE;
    marg_order_new.items++;
  }
  marg_order_new.abs_order_map[last_state_to_marg] = (marg_order_new.total_size, POSE_VEL_BIAS_SIZE);
  marg_order_new.total_size += POSE_VEL_BIAS_SIZE;
  marg_order_new.items++;

  marg_data.H = marg_H_new;
  marg_data.b = marg_b_new;
  marg_data.order = marg_order_new;
  assert(marg_data.H.cols() == marg_data.order.total_size);

  // Quadratic prior and "delta" of the current state to the original
  // linearization point give cost function
  //
  //    P(x) = 0.5 || J*(delta+x) + r ||^2.
  //
  // For marginalization this has been linearized at x=0 to give
  // linearization
  //
  //    P(x) = 0.5 || J*x + (J*delta + r) ||^2,
  //
  // with Jacobian J and residual J*delta + r.
  //
  // After marginalization, we recover the original form of the
  // prior. We are left with linearization (in sqrt form)
  //
  //    Pnew(x) = 0.5 || Jnew*x + res ||^2.
  //
  // To recover the original form with delta-independent r, we set
  //
  //    Pnew(x) = 0.5 || Jnew*(delta+x) + (res - Jnew*delta) ||^2,
  //
  // and thus rnew = (res - Jnew*delta).

  VecX delta;
  computeDelta(marg_data.order, delta);
  marg_data.b -= marg_data.H * delta;

  if (config.vio_debug || config.vio_extended_logging) {
    nullspace_marg_data.b -= nullspace_marg_data.H * delta;
    logMargNullspace(); // MARK // TODO@mateosss
  }
}

```
